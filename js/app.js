'use-strict'
const progress = document.querySelector('.progress-bar')
const containerBg = $('.container-img-bg')
let contador = 1;


// -- BARRA DE PROGRESO --
window.onscroll = () => scrollProgress()
const scrollProgress = () => {
    var winScroll = document.documentElement.scrollTop
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight
    var scrolled = (winScroll / height) * 100
    progress.style.width = `${scrolled}%`

}

// --- CAMBIAR IMG DE FONDO CADA 4SEG --
$(document).ready(function() {
    setInterval( () => changeBackground(), 4000)
})

// const changeBackground = () => {
//     contador++;
//     const ruta_img = `../img/backgrounds/${contador.toString()}.jpg`
//     containerBg.css('background-image', `url('${ruta_img}')`);
//     if (contador >= 7) contador = 0
// }